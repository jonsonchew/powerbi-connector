﻿// This file contains your Data Connector logic
section TestOAuth;

//Constants
redirect_uri = "https://oauth.powerbi.com/views/oauthredirect.html";
client_id = "4e4dc9ccb0b23499a743";
client_secret="d8b53b78a9dcb9c217922bb434a95e334fdcab1e";
windowWidth = 1200;
windowHeight = 1000;

[DataSource.Kind="TestOAuth", Publish="TestOAuth.Publish"]
shared TestOAuth.Contents = Value.ReplaceType(OAuth.Contents, OAuth.Type);

OAuth.Type = type function(
     url as (type text meta [
        Documentation.FieldCaption = "URL",
        Documentation.FieldDescription = "Text to display",
        Documentation.AllowedValues = {"https://login-test.loc8.com"}
    ]),
    entityType as (type text meta [
        Documentation.FieldCaption = "Entity Type",
        Documentation.FieldDescription = "Entity Type for reports",
        Documentation.AllowedValues = {"ASSET","LOC8EVENT","WORK_ORDER_EXPENSE"}
    ]),
    reportName as (type text meta [
        Documentation.FieldCaption = "Report Name",
        Documentation.FieldDescription = "Report Name",
        Documentation.SampleValues = {"reportA"}
    ])
)as any;

/*explorerResults = Json.Document(explorerResJson),
                                explorerResultsTable = Table.FromRecords(explorerResults),*/
OAuth.Contents = (url as text, entityType as text, reportName as text) as table =>
    let
        content = if Text.End(url,1) = "/" then Web.Contents(url&"api/users/me/tenants")
                  else Web.Contents(url&"/api/users/me/tenants"),
        link = GetNextLink(content),
        json = Json.Document(content),
        table = Table.FromRecords(json),
        selectedTrueTable = Table.SelectRows(table, each [selected] = true),
        UUID = List.First(
            Table.Column(selectedTrueTable, "id")
        ),
        host = List.First(
            Table.Column(selectedTrueTable, "host")
        ),
        explorerRes=
            let
                reportSummaryJson = Web.Contents(host&"/rest/v1/reports?explorerType="&entityType&"&query="&reportName&"&exact=true&incudeTrashed=false",[
                    Headers=[#"Content-type" = "application/json", #"Accept" = "application/json", #"X-Client"= UUID]
                ]), 
                reportSummary = Json.Document(reportSummaryJson),
                reportSummaryTable = Table.FromRecords(reportSummary),
                reportSummaryUUID = List.First(
                    Table.Column(reportSummaryTable, "uuid")
                ),
                reportJsonRes = 
                    let
                        reportJson = Web.Contents(host&"/rest/v1/reports/"&reportSummaryUUID,[
                            Headers=[#"Content-type" = "application/json", #"Accept" = "application/json", #"X-Client"= UUID]
                        ]), 
                        report = Json.Document(reportJson),
                        explorerViewJson= Text.FromBinary(Json.FromValue(report[explorer])),//tostring
                        postContents= explorerViewJson,

                    //---------------------check fieldtype--------------------
                        explorerFieldJson = Web.Contents(host&"/rest/v1/explorer/"&entityType&"/fields?includeInvisible=false",[
                            Headers=[#"Content-type" = "application/json", #"Accept" = "application/json", #"X-Client"= UUID]
                        ]), 
                        explorerField = Json.Document(explorerFieldJson),
                        //LOOP CHECK DATATYPE/DATA DEFINITION
                        dataTypeList= fnDataType(report[explorer], explorerField),
                    //---------------------end check fieldtype----------------

                        explorerResData =
                            let 
                                explorerResJson = Web.Contents(host&"/rest/v1/explorer/results",[
                                    Headers = [#"Content-type" = "application/json"],
                                    Content = Text.ToBinary(postContents)
                                ])
                            in
                                explorerResJson,
                        explorerResDataJson = Json.Document(explorerResData,65001),
                        explorerResDataTable = Table.FromRecords(explorerResDataJson[results]),
                        /*explorerResDataTableDateFormatted = Table.TransformColumnTypes(explorerResDataTable,
                            {
                                {"CREATED_DATE", type datetime}, 
                                {"LAST_UPDATE_DATE", type datetime},
                                {"LAST_MAINTENANCE_DATE", type datetime}
                            }
                        )*/
                        explorerResDataTableDateFormatted = Table.TransformColumnTypes(explorerResDataTable,
                            dataTypeList
                        )
                    in
                        explorerResDataTableDateFormatted
            in  
                reportJsonRes
    in 
        explorerRes;

//LOOP CHECK DATATYPE/DATA DEFINITION
fnDataType = (recordA, recordB) =>
    let
    tableA = Table.PrefixColumns(Table.FromRecords(recordA[columns]),"tableA"),
    tableB = Table.PrefixColumns(Table.FromRecords(recordB),"tableB"),
    tableAB= Table.Join(tableA,"tableA.field",tableB,"tableB.name",JoinKind.LeftOuter),
    tableReform = Table.AddColumn(tableAB,"dataType",
        each
            if [tableA.dataFieldDefinition] = null then
               if [tableB.type] = "DATE" or [tableB.type] = "DATE_TIME" then
                    type datetime
               else if [tableB.type] = "CURRENCY" then
                    Currency.Type
               else
                    //[tableB.type]
                    type text
            else
               if [tableA.dataFieldDefinition][type] = "DATE" or [tableA.dataFieldDefinition][type] = "DATE_TIME" then
                    type datetime
               else if [tableA.dataFieldDefinition][type] = "CURRENCY" then
                    Currency.Type
               else
                    //[tableA.dataFieldDefinition][type]
                    type text
    ),
    trimTableAB = Table.RemoveColumns(tableReform,{"tableA.name","tableA.userLabel","tableA.dataFieldDefinition","tableB.type","tableB.name","tableB.visible"}),
    listFieldTypeAB = Table.ToRows(trimTableAB)
    //listFieldTypeAB=Table.ToList(trimToRowTableAB, Combiner.CombineTextByDelimiter(","))
    in
        listFieldTypeAB
;

GetNextLink = (response, optional request) =>
    let
        // extract the "Link" header if it exists
        link = Value.Metadata(response)[Headers][#"Link"]?,
        links = Text.Split(link, ","),
        splitLinks = List.Transform(links, each Text.Split(Text.Trim(_), ";")),
        next = List.Select(splitLinks, each Text.Trim(_{1}) = "rel=""next"""),
        first = List.First(next),
        removedBrackets = Text.Range(first{0}, 1, Text.Length(first{0}) - 2)
    in
        try removedBrackets otherwise null;

// Data Source Kind description
TestOAuth = [
    //TestConnection = (dataSourcePath) => {"TestOAuth.Contents", dataSourcePath}, 
    TestConnection = (dataSourcePath) =>
        let
            json = Json.Document(dataSourcePath),
            url = json[url],
            entityType = json[entityType],
            reportName = json[reportName]
            in
                {"TestOAuth.Contents", url, entityType, reportName},
    Authentication = [
        OAuth =[
            StartLogin = StartLogin,
            FinishLogin = FinishLogin,
            Label = Extension.LoadString("DataSourceLabel")
        ]
    ]
];

StartLogin = (resourceUrl, state, display) =>
    let
        AuthorizeUrl = "https://login-test.loc8.com/oauth/authorize?" & Uri.BuildQueryString([
            response_type = "code",
            scope = "self:read",
            client_id = client_id,
            client_secret = client_secret,
            redirect_uri = redirect_uri
        ])
    in
        [
            LoginUri = AuthorizeUrl,
            CallbackUri = redirect_uri,
            WindowHeight = windowHeight,
            WindowWidth = windowWidth,
            Context = null
        ];

FinishLogin = (context, callbackUri, state) =>
    let 
        Parts = Uri.Parts(callbackUri)[Query]
    in
       TokenMethod(Parts[code]);


TokenMethod = (code) =>
    let
        Response = Web.Contents("https://login-test.loc8.com/oauth/token",[
            Content = Text.ToBinary(Uri.BuildQueryString([
                client_id = client_id,
                client_secret = client_secret,
                code = code,
                grant_type = "authorization_code",
                redirect_uri = redirect_uri
            ])),
            Headers=[#"Content-type" = "application/x-www-form-urlencoded", #"Accept" = "application/json"],
            ManualStatusHandling = {400,404,500,403}
        ]),
        Body = Json.Document(Response),
        Result = if Record.HasFields(Body, {"error", "error_description"}) then
                error Error.Record(Body[error], Body[error_description], Body)
            else
                Body
    in
        Result;



// Data Source UI publishing description
TestOAuth.Publish = [
    Beta = true,
    Category = "Other",
    ButtonText = { Extension.LoadString("ButtonTitle"), Extension.LoadString("ButtonHelp") },
    LearnMoreUrl = "https://powerbi.microsoft.com/",
    SourceImage = TestOAuth.Icons,
    SourceTypeImage = TestOAuth.Icons
];

TestOAuth.Icons = [
    Icon16 = { Extension.Contents("TestOAuth16.png"), Extension.Contents("TestOAuth20.png"), Extension.Contents("TestOAuth24.png"), Extension.Contents("TestOAuth32.png") },
    Icon32 = { Extension.Contents("TestOAuth32.png"), Extension.Contents("TestOAuth40.png"), Extension.Contents("TestOAuth48.png"), Extension.Contents("TestOAuth64.png") }
];
